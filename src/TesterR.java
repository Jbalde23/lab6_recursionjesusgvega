import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class TesterR {

	
	@Test
	public void testIncreasingPairs() {
		SLL<Integer> list = new SLL<Integer>();
		list.addFirst(2);
		list.addFirst(1);
		list.addFirst(9);
		list.addFirst(4);
		list.addFirst(10);
		list.addFirst(3);
		list.addFirst(5);
		list.addFirst(5);
		String values = "5 5 3 10 4 9 1 2";
		assertEquals("Should have a list with (5, 5, 3, 10, 4, 9, 1, 2)", values, list.toString().trim());
		ArrayList<Pair<Integer>> data = list.consecutiveIncreasingPairs();
		Pair<Integer> a = new Pair(3,10);
		Pair<Integer> b = new Pair(4,9); //Test first pairs
		
		
		assertEquals("First 3", a.first(), data.get(0).first());
		assertEquals("Second 10", a.second(), data.get(0).second());
		assertEquals("First 4", b.first(), data.get(1).first());
		assertEquals("Second 9", b.second(), data.get(1).second());
		
	}
	
		@Test
		public void testReverse() {
			SLL<Integer> list = new SLL<Integer>();
			list.addFirst(8);
			list.addFirst(5);
			list.addFirst(3);
			list.addFirst(3);
			list.addFirst(7);
			list.addFirst(9);
			String values = "9 7 3 3 5 8";
			assertEquals("Should have a list with (9, 7, 3, 3, 5, 8)", values, list.toString().trim());
			list.reverse();
			String valuesRev = "8 5 3 3 7 9";
			assertEquals("Should have a list with (8, 5, 3, 3, 7, 9)", valuesRev, list.toString().trim());
		}
		
		
	

	

}
