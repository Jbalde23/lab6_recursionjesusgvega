
public class Fibonacci {
	
	public void main(String args[]){
		
		System.out.println(fib(50));
	}
	
	long fib(int n) { 
		   if (n == 1 || n == 0) return 1; 
		   else return fib(n-1) + fib(n-2); 
		} 


	Pair<Integer> fibR(int n){ 
		if (n==0 || n==1) return new Pair<Integer>(1,1); 
		Pair<Integer> p = fibR(n-1); 
		return new Pair<Integer>(p.first() + p.second(), p.first());
	}

	
	

}
