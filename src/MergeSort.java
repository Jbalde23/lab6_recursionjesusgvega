import java.util.ArrayList;


public class MergeSort{

	
	public static <E> void mergeSort(Queue<E> qu) {
		
		if (qu.size() > 1) {     
		      Queue<E> q1, q2; 
		      q1 = new ArrayQueue<>(); 
		      q2 = new ArrayQueue<>();  //SPlit

		      int n = qu.size(); 
		      for (int i=0; i<n/2; i++) 
		          q1.enqueue(qu.dequeue()); 
		      while (!qu.isEmpty())
		          q2.enqueue(qu.dequeue()); 

		      mergeSort(q1);    
		      mergeSort(q2);    
		      
		      while (!q1.isEmpty() && !q2.isEmpty())
		          if (((Comparable<E>) q1.first()).compareTo(q2.first()) <= 0)
		             qu.enqueue(q1.dequeue()); 
		          else 
		             qu.enqueue(q2.dequeue()); 
		    
		      Queue<E> q = (!q1.isEmpty() ? q1 : q2);  
		      while (!q.isEmpty())
		          qu.enqueue(q.dequeue()); 
		     } 
		}


	

		
	
	public static  <E> void mergeSortArr(ArrayList<E> qu) {
		if (qu.size() > 1) {   
		      Queue<E> q1, q2; 
		      q1 = new ArrayQueue<>(); 
		      q2 = new ArrayQueue<>(); //Split list

		      int n = qu.size(); 
		      for (int i=0; i<n/2; i++) 
		          q1.enqueue(qu.get(i)); 
		      for(int j = n/2 ;j<n ;j++)
		          q2.enqueue(qu.get(j)); 
		     
		      mergeSort(q1);    
		      mergeSort(q2);    

		      while (!q1.isEmpty() && !q2.isEmpty())
		          if (((Comparable<E>) q1.first()).compareTo(q2.first()) <= 0)
		             qu.add(q1.dequeue()); 
		          else 
		             qu.add(q2.dequeue()); 
		      
		      Queue<E> q = (!q1.isEmpty() ? q1 : q2); 
		      while (!q.isEmpty())
		          qu.add(q.dequeue()); 
		     } 
		}

	}
